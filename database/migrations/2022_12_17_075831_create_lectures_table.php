<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lectures', function (Blueprint $table) {
            $table->id();
            $table->string("code");
            $table->string("name")->default("");
            $table->integer("year");
            $table->string("undergraduate")->default("");
            $table->string("department")->default("");
            $table->string("course")->default("");
            $table->string("major")->default("");
            $table->string("lecture_period")->default("");
            $table->string("lecture_week_time")->default("");
            $table->string("representative_teacher")->default("");
            $table->timestamps();
            $table->unique(["code", "year"]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lectures');
    }
};
