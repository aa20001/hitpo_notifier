<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scrap_histories', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("type_id");
            $table->dateTime("datetime")->useCurrent();
            $table->timestamps();

            $table->foreign("type_id")->references("id")->on("scrap_types");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scrap_histories');
    }
};
