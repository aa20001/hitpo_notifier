<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lecture_notifies', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("type_id");
            $table->string("undergraduate");
            $table->string("department");
            $table->integer("target_year");
            $table->date("date");
            $table->string("time");
            $table->string("lecture_name");
            $table->string("classroom")->nullable();
            $table->string("remarks")->nullable();
            $table->unsignedBigInteger("lecture_id");
            $table->timestamps();

            $table->unique(["target_year", "date", "time", "type_id", "lecture_id"]);
            $table->foreign("lecture_id")->references("id")->on("lectures");
            $table->foreign("type_id")->references("id")->on("lecture_notify_types");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lecture_notifies');
    }
};
