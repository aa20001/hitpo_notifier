<?php

namespace App\Http\Controllers;

use App\Models\Lecture;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SubscribeController extends Controller
{
    public function __invoke(Request $request) {
        if (!Auth::check()) { return redirect("home"); }

        if (!$request->has("code") && $request->get("code")) abort(404, "parameter error");

        foreach (explode(",", $request->get("code")) as $code) {
            $code = trim($code);
            $lecture = Lecture::firstOrCreate([
                'code' => $code,
                'year' => intval(date('Y')) - (intval(date("m"))<=3)
            ], []);

            $request->user()->lectures()->syncWithoutDetaching($lecture);
        }


        return redirect("home");
    }
}
