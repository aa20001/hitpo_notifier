<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function __invoke(Request $request) {
        if (Auth::check()) {
            return view("home", ["lectures" => Auth::user()->lectures->filter(function ($value, $key) {
                return $value->year == intval(date('Y')) - (intval(date("m"))<=3);
            })]);
        }
        return view("home");
    }
    //
}
