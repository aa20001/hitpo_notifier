<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Lecture;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CourseController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function __invoke(Request $request)
    {
        if (!Auth::check() || !Auth::user()->is_admin) {
            return abort(401);
        }

        $courses = Lecture::all();
        return view("admin/courses", ["courses" => $courses]);
    }

    public function create(Request $request) {
        if ($request->isMethod("get")) {
            return view("admin/createCourse");
        } elseif ($request->isMethod("post")) {
            return redirect("admin");
        }
    }
}
