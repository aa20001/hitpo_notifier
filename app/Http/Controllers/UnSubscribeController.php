<?php

namespace App\Http\Controllers;

use App\Models\Lecture;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UnSubscribeController extends Controller
{
    public function __invoke(Request $request) {
        if (!Auth::check()) { return redirect("home"); }

        if (!$request->has("code") && $request->get("code")) abort(404, "parameter error");

        $lecture = Lecture::firstOrCreate([
            'code' => $request->get("code"),
            'year' => intval(date('Y')) - (intval(date("m"))<=3)
        ], []);

        $request->user()->lectures()->detach($lecture);
        return redirect("home");
    }
}
