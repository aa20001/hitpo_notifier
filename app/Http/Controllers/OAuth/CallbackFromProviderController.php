<?php

namespace App\Http\Controllers\OAuth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class CallbackFromProviderController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
    public function __invoke(Request $request)
    {
        $hit_user = Socialite::driver('azure')->user();
        $user = User::updateOrCreate([
            'uuid' => $hit_user->getId(),
        ], [
            'email' => $hit_user->getEmail(),
            'name' => $hit_user->getName(),
            "is_admin" => false,
        ]);

        Auth::login($user);
        return redirect('home');
        //
    }
}
