<?php

namespace App\Http\Controllers;

use App\Models\ScrapHistory;
use Illuminate\Http\Request;

class CSLectureCacheController extends Controller
{
    //
    public function __invoke() {
        $history = ScrapHistory::with("notifies")->latest("datetime")->first();
        if ($history) {
            return view("CSLecture", ["history"=>$history]);

        } else {
            return view("CSLecture", ["history"=>false]);
        }

    }
}
