<?php

namespace App\Http\Controllers;

use App\Models\LectureNotify;
use App\Models\LectureNotifyType;
use Illuminate\Http\Request;

const HISTORY_TAKE_SELECTIONS = [10, 20, 50, 100];

class CSLectureHistoryController extends Controller
{
    //
    public function __invoke(Request $request) {
        if ($request->has("type") && is_numeric($request->get("type"))) {
            $type_id = intval($request->get("type"));
            $type = LectureNotifyType::find($type_id);
            if ($type) {
                $n_rows = ($request->has("type")&&intval($request->get("nrows")))?intval($request->get("nrows")):HISTORY_TAKE_SELECTIONS[0];
                if (!in_array($n_rows, HISTORY_TAKE_SELECTIONS)) {
                    $n_rows = HISTORY_TAKE_SELECTIONS[0];
                }
                $notifies = LectureNotify::where("type_id", '=', $type->id)->latest("date")->limit($n_rows);
                return view("CSLectureHistory", ["notifies"=>$notifies, "type"=>$type, "n_rows"=>$n_rows, "result_lengths"=>HISTORY_TAKE_SELECTIONS]);
            }
        }
        return view("CSLectureHistory", ["notifies"=>false, "type"=>false, "n_rows"=>false, "result_lengths"=>HISTORY_TAKE_SELECTIONS]);
    }
}
