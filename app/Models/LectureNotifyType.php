<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LectureNotifyType extends Model
{
    use HasFactory;

    protected $table = "lecture_notify_types";

    protected $fillable = [
        "name"
    ];

    public function notifies(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(LectureNotify::class, "type_id");
    }

}
