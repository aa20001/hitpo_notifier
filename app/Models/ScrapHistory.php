<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ScrapHistory extends Model
{
    use HasFactory;
    protected $table = "scrap_histories";

    protected $fillable = [
        "type_id",
        "datetime",
    ];

    public function type(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(ScrapType::class);
    }

    public function notifies(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(LectureNotify::class, "lecture_notify_scrap_history");
    }
}
