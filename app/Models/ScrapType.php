<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ScrapType extends Model
{
    use HasFactory;
    protected $table = "scrap_types";

    protected $fillable = [
        "name",
    ];

    public function histories() {
        return $this->hasMany(ScrapHistory::class, "type_id", );
    }
}
