<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LectureNotify extends Model
{
    use HasFactory;

    protected $table = "lecture_notifies";

    protected $fillable = [
        "type_id",
        "undergraduate",
        "department",
        "target_year",
        "date",
        "time",
        "lecture_id",
        "lecture_name",
        "classroom",
        "remarks",
    ];

    public function type(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(LectureNotifyType::class);
    }

    public function lecture(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Lecture::class);
    }

    public function histories(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(ScrapHistory::class, "lecture_notify_scrap_history");
    }
}
