<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lecture extends Model
{
    use HasFactory;

    protected $table = "lectures";
    protected $fillable = [
        'code',
        'name',
        'year',
        "undergraduate",
        "department",
        "course",
        "major",
        "lecture_period",
        "lecture_week_time",
        "representative_teacher"
    ];

    public function users(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(User::class);
    }

    public function notifies(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(LectureNotify::class);

    }
}
