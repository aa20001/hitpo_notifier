<?php

namespace App\Console\Commands;

use App\Models\Lecture;
use DOMDocument;
use DOMElement;
use DOMXPath;
use Illuminate\Console\Command;

class ScrapSyllabus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'scrap:Syllabus';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    private array $saveCookieFile;

    const HTTEP_REAUEST_HEADER = [
        'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
        'Accept-Language: ja,en-US;q=0.9,en;q=0.8',
        'Cache-Control: no-cache',
        'Connection: keep-alive',
        "Origin: https://hitpo.it-hiroshima.ac.jp",
        'Pragma: no-cache',
        "Referer: https://hitpo.it-hiroshima.ac.jp/PfStudent/Login",
        'Sec-Fetch-Dest: document',
        'Sec-Fetch-Mode: navigate',
        'Sec-Fetch-Site: same-origin',
        'Sec-Fetch-User: ?1',
        'Sec-Fetch-User: ?1',
        "Upgrade-Insecure-Requests: 1",
        "User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36",
        'sec-ch-ua: "Not?A_Brand";v="8", "Chromium";v="108", "Google Chrome";v="108"',
        'sec-ch-ua-mobile: ?0',
        'sec-ch-ua-platform: "macOS"'
    ];
    const SCRAP_SLEEP = 1;
    const TABLE_MAXIMUM_ROWS = 1000;

    public function __construct()
    {
        parent::__construct();
        $this->saveCookieFile = stream_get_meta_data(tmpfile());
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->register();

        return Command::SUCCESS;
    }

    public function register(){
        $tables = $this->getTables();
        $table_data = $this->parseTables($tables);
        foreach ($table_data as $data) {
            $undergraduate = "";
            $department = "";
            $course = "";
            $major = "";
            $target_stu_info = explode(" ", $data["学部・学科"]);
            if (count($target_stu_info) == 2) {
                $undergraduate = trim($target_stu_info[0]);
                $department = trim($target_stu_info[1]);
            } elseif (count($target_stu_info) == 3) {
                $department = trim($target_stu_info[0]);
                $course = trim($target_stu_info[1]);
                $major = trim($target_stu_info[2]);
            }
            Lecture::updateOrCreate([
                "year"=>intval(trim(str_replace("年度", "", $data["授業開講年度"]))),
                "code"=>trim($data["授業コード"]),
            ], [
                "name"=>trim($data["授業名称"]),
                "undergraduate" => $undergraduate,
                "department"=>$department,
                "course"=>$course,
                "major"=>$major,
                "lecture_period"=>$data["履修期"],
                "lecture_week_time"=>$data["曜時"],
                "representative_teacher"=>$data["代表教員名"]
            ]);
        }
    }
    public function getTables(): array
    {
        $entrypoint = "https://sv-hit-gak03.it-hiroshima.ac.jp/uniasv2/UnSSOLoginControlFree?REQ_ACTION_DO=/AGA030.do&REQ_PRFR_FUNC_ID=T_AGA030_1&CRR_FLG=1";
        $document_res = $this->get($entrypoint);
        $document = new DOMDocument();
        @$document->loadHTML($document_res);
        $params = $this->getDefaultParams($document);
        $params["ESearch"] = "検索(S)";

        $tmp_result = $this->post("https://sv-hit-gak03.it-hiroshima.ac.jp/uniasv2/AGA030PSC01EventAction.do", $params);
        @$document->loadHTML($tmp_result);
        $params = $this->getDefaultParams($document);
        $params["EDispNumberSet.x"] = 12;
        $params["EDispNumberSet.y"] = 13;
        $params["selCasePerPage"] = self::TABLE_MAXIMUM_ROWS;
        $tables = [];
        do {
            $page = $this->post("https://sv-hit-gak03.it-hiroshima.ac.jp/uniasv2/AGA030PLS01EventAction.do", $params);
            @$document->loadHTML($page);
            $current_page = intval($document->getElementById("hdnCurrentPage")->getAttribute("value"));
            $maximum_page = intval($document->getElementById("hdnTotalPage")->getAttribute("value"));

            $table = $document->getElementsByTagName("table")->item(0);
            $tables[] = $table;

            $params = $this->getDefaultParams($document);
            $params["ENext.x"] = 11;
            $params["ENext.y"] = 14;
        } while ($current_page != $maximum_page);
//        } while ($current_page != 2);
        return $tables;
    }

    public function parseTables($tables): array
    {
        $data = [];
        foreach ($tables as $table) {
            $data = array_merge($data, $this->parseTable($table));
        }
        return $data;

    }

    private function parseTable(DOMElement $table): array
    {
        $idx_names = [];
        $data = [];

        foreach ($table->getElementsByTagName("tr") as $index => $row) {
            if ($index == 0) {
                foreach ($row->getElementsByTagName("th") as $name) {
                    $idx_names[] = $name->textContent;
                }
            } else {
                $tmp = [];
                $items = array_merge(iterator_to_array($row->getElementsByTagName("th")), iterator_to_array($row->getElementsByTagName("td")));
                foreach ($items as $key => $value) {
                    if ($value->getElementsByTagName("input")) {
                        foreach ($value->getElementsByTagName("input") as $input) {
                            if ($input->hasAttribute("type") && $input->getAttribute("type") == "hidden" && $input->hasAttribute("value")) {
                                $tmp[$idx_names[$key]] = $input->getAttribute("value");
                            }
                        }
                    } else {
                        $tmp[$idx_names[$key]] = $value->textContent;
                    }
                }
                $data[] = $tmp;
            }
        }
        return $data;
    }

    public function post($url, $params) {
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($params));
        curl_setopt($curl, CURLOPT_COOKIEJAR, $this->saveCookieFile['uri']);
        curl_setopt($curl, CURLOPT_COOKIEFILE, $this->saveCookieFile['uri']);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
//        curl_setopt($curl, CURLOPT_VERBOSE, 1);
        curl_setopt($curl, CURLOPT_HTTPHEADER, self::HTTEP_REAUEST_HEADER);
        $result = curl_exec($curl);
        curl_close($curl);
        sleep(self::SCRAP_SLEEP);
        return $result;
    }

    public function get($url): bool|string
    {
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_COOKIEJAR, $this->saveCookieFile['uri']);
        curl_setopt($curl, CURLOPT_COOKIEFILE, $this->saveCookieFile['uri']);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
//        curl_setopt($curl, CURLOPT_HTTPHEADER, self::HTTEP_REAUEST_HEADER);
        $result = curl_exec($curl);
        curl_close($curl);
        sleep(self::SCRAP_SLEEP);
        return $result;
    }

    public function getDefaultParams(DOMDocument|DOMElement $document) {
        $params = [];
        foreach ($document->getElementsByTagName("input") as $input) {
            // ToDo: Support input types: Radio, Checkbox, File, Submit, Reset
            if (!$input->hasAttribute("name")) continue;
            if ($input->hasAttribute("type")) {
                switch (strtolower($input->getAttribute("type"))) {
                    case "text":
                    case "password":
                    case "hidden":
                        if ($input->hasAttribute("value")) {
                            $params[$input->getAttribute("name")] = $input->getAttribute("value");
                        } else {
                            $params[$input->getAttribute("name")] = null;
                        }
                        break;
                    case "checkbox":
                        if ($input->hasAttribute("value") && $input->hasAttribute("checked")) {
                            if ($input->getAttribute("name")->endsWith("[]")) {
                                $params[$input->getAttribute("name")][] = $input->getAttribute("value");
                            } else {
                                $params[$input->getAttribute("name")] = $input->getAttribute("value");
                            }
                        } else {
                            $params[$input->getAttribute("name")] = null;
                        }
                        break;
                }
            }
        }
        foreach ($document->getElementsByTagName("textarea") as $input) {}
        foreach ($document->getElementsByTagName("select") as $select) {
            if (!$select->hasAttribute("name")) continue;
            foreach ($select->getElementsByTagName("option") as $index => $option) {
                if ($option->hasAttribute("selected") || $index==0) {
                    if ($option->hasAttribute("value")) {
                        $params[$select->getAttribute("name")] = $option->getAttribute("value");
                    } else {
                        $params[$select->getAttribute("name")] = null;
                    }
                }
            }
        }
        return $params;
    }
}
