<?php

namespace App\Console\Commands;

use App\Models\Lecture;
use App\Models\LectureNotify;
use App\Models\LectureNotifyType;
use App\Models\ScrapHistory;
use App\Models\ScrapType;
use ArrayObject;
use DateTime;
use DOMDocument;
use DOMElement;
use Symfony\Component\Mailer\Exception\TransportException;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class ScrapCSLecture extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'scrap:CSLecture';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';
    private array $saveCookieFile;

    public function __construct()
    {
        parent::__construct();
        $this->saveCookieFile = stream_get_meta_data(tmpfile());
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        $this->register();
        return Command::SUCCESS;
    }

    private function register()
    {
        $this->login();
        $content = $this->getCSLecture();
//        $content = file_get_contents("test.cslecture.html");
        if (!$content) return;

        $dom_document = new DOMDocument(encoding: "utf-8");
        @$dom_document->loadHTML($content);

        $history_type = ScrapType::firstOrCreate([
            "name"=>"CSLecture"
        ], []);
        $history = ScrapHistory::create(["type_id"=>$history_type->id]);

        $this->registerMoveClassroom($dom_document, $history);
        $this->registerCancelLecture($dom_document, $history);
        $this->registerSupplementaryLecture($dom_document, $history);

    }

    public function sendNotify($notify)
    {
        if (empty($notify->lecture->users)) return;
        try {
            Mail::raw("あなたの受講している講義で通知があります
通知種別: {$notify->type->name}
学部: $notify->undergraduate
学科: $notify->department
講義名: $notify->lecture_name
日付: {$notify->date}
時限: $notify->time".
($notify->classroom?"教室: $notify->classroom\n":"\n").
"備考: $notify->remarks

unsubscribe: ". route("unsubscribe", ["code" => $notify->lecture->code]), function ($message) use ($notify) {
                $message->from(env("MAIL_FROM_ADDRESS"), env("MAIL_FROM_NAME"));
//                $message->to("test@freemail.dau.jp");
                $message->to([]);
                $message->subject("講義情報が変更されました");
                foreach ($notify->lecture->users as $user) {
                    $message->bcc($user->email);
                }
            });
        } catch (TransportException) {
        }
    }

    public function registerSupplementaryLecture(DOMDocument $document, $history)
    {
        $supplementary_lecture_root = $document->getElementById("MainContent_SupLectureTitle");
        if (!$supplementary_lecture_root) return;
        $data = $this->parseTable($supplementary_lecture_root);

        $notify_type = LectureNotifyType::updateOrCreate([
            "name" => "補講",
        ]);

        $new_notify = $this->registerNotifies($data, $notify_type, $history);

        foreach ($new_notify as $notify) {
            $this->sendNotify($notify);
        }
    }

    public function registerCancelLecture(DOMDocument $document, $history)
    {
        $cancel_lecture_root = $document->getElementById("MainContent_CancelLectureTitle");
        if (!$cancel_lecture_root) return;
        $data = $this->parseTable($cancel_lecture_root);


        $notify_type = LectureNotifyType::updateOrCreate([
            "name" => "休講",
        ]);

        $new_notify = $this->registerNotifies($data, $notify_type, $history);

        foreach ($new_notify as $notify) {
            $this->sendNotify($notify);
        }
    }

    public function registerMoveClassroom(DOMDocument $document, $history)
    {
        $move_classroom_root = $document->getElementById("MainContent_MoveLectureTitle");
        if (!$move_classroom_root) return;
        $data = $this->parseTable($move_classroom_root);

        $notify_type = LectureNotifyType::updateOrCreate([
            "name" => "教室変更",
        ]);

        $new_notify = $this->registerNotifies($data, $notify_type, $history);

        foreach ($new_notify as $notify) {
            $this->sendNotify($notify);
        }
    }

    private function parseTable(DOMElement $table_root): ArrayObject
    {
        $idx_names = new ArrayObject();
        $data = new ArrayObject();

        foreach ($table_root->getElementsByTagName("tr") as $index => $row) {
            if ($index == 0) {
                foreach ($row->getElementsByTagName("th") as $name) {
                    $idx_names->append($name->textContent);
                }
            } else {
                $tmp = [];
                foreach ($row->getElementsByTagName("td") as $key => $value) {
                    $tmp[$idx_names[$key]] = $value->textContent;
                }
                $data->append($tmp);
            }
        }
        return $data;
    }


    private function login()
    {
        $curl = curl_init("https://hitpo.it-hiroshima.ac.jp/PfStudent/Login?target_url=%2fPfStudent%2fPortal");
        curl_setopt($curl, CURLOPT_COOKIEJAR, $this->saveCookieFile['uri']);
        curl_setopt($curl, CURLOPT_COOKIEFILE, $this->saveCookieFile['uri']);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, [
            'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
            'Accept-Language: ja,en-US;q=0.9,en;q=0.8',
            'Cache-Control: no-cache',
            'Connection: keep-alive',
            "Origin: https://hitpo.it-hiroshima.ac.jp",
            'Pragma: no-cache',
            "Referer: https://hitpo.it-hiroshima.ac.jp/PfStudent/Login",
            'Sec-Fetch-Dest: document',
            'Sec-Fetch-Mode: navigate',
            'Sec-Fetch-Site: same-origin',
            'Sec-Fetch-User: ?1',
            'Sec-Fetch-User: ?1',
            "Upgrade-Insecure-Requests: 1",
            "User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36",
            'sec-ch-ua: "Not?A_Brand";v="8", "Chromium";v="108", "Google Chrome";v="108"',
            'sec-ch-ua-mobile: ?0',
            'sec-ch-ua-platform: "macOS"'
        ]);
        $result = curl_exec($curl);
        curl_close($curl);
        $dd = new DOMDocument(encoding: "utf-8");
        @$dd->loadHTML($result);
        $params = [];
        foreach ($dd->getElementsByTagName("input") as $value) {
            $params[$value->getAttribute("name")] = $value->getAttribute("value");
        }

        $params['ctl00$MainContent$login_id'] = env("HITPO_USERNAME");
        $params['ctl00$MainContent$login_password'] = env("HITPO_PASSWORD");
        $params['ctl00$MainContent$LoginButton.x'] = "121";
        $params['ctl00$MainContent$LoginButton.y'] = "15";


        $curl = curl_init("https://hitpo.it-hiroshima.ac.jp/PfStudent/Login?target_url=%2fPfStudent%2fPortal");
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($params));
        curl_setopt($curl, CURLOPT_COOKIEJAR, $this->saveCookieFile['uri']);
        curl_setopt($curl, CURLOPT_COOKIEFILE, $this->saveCookieFile['uri']);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
//        curl_setopt($curl, CURLOPT_VERBOSE, 1);
        curl_setopt($curl, CURLOPT_HTTPHEADER, [
            'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
            'Accept-Language: ja,en-US;q=0.9,en;q=0.8',
            'Cache-Control: no-cache',
            'Connection: keep-alive',
            "Origin: https://hitpo.it-hiroshima.ac.jp",
            'Pragma: no-cache',
            "Referer: https://hitpo.it-hiroshima.ac.jp/PfStudent/Login",
            'Sec-Fetch-Dest: document',
            'Sec-Fetch-Mode: navigate',
            'Sec-Fetch-Site: same-origin',
            'Sec-Fetch-User: ?1',
            'Sec-Fetch-User: ?1',
            "Upgrade-Insecure-Requests: 1",
            "User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36",
            'sec-ch-ua: "Not?A_Brand";v="8", "Chromium";v="108", "Google Chrome";v="108"',
            'sec-ch-ua-mobile: ?0',
            'sec-ch-ua-platform: "macOS"'
        ]);
        $result = curl_exec($curl);
        curl_close($curl);
    }

    private function getCSLecture(): bool|string
    {
        $curl = curl_init('https://hitpo.it-hiroshima.ac.jp/PfStudent/CSLecture');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_COOKIEFILE, $this->saveCookieFile['uri']);
//        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($curl);
        curl_close($curl);
        return $result;
    }

    /**
     * @param ArrayObject $data
     * @param $notify_type
     * @return ArrayObject $new_notify
     */
    private function registerNotifies(ArrayObject $data, $notify_type, $history): ArrayObject
    {
        $new_notify = new ArrayObject();
        foreach ($data as $row) {
            $date = strtotime(substr($row["日付"], 0, 10));
            $lecture = Lecture::updateOrCreate([
                "code" => $row["授業コード"],
                "year" => intval(date("Y", $date)) - (intval(date("m", $date)) < 4)
            ], [
            ]);
            $create_data = [
                "undergraduate" => $row["学部"],
                "department" => $row["学科"],
                "lecture_name" => $row["授業名"],
                "remarks" => $row["備考"],
            ];
            if (array_key_exists("教室名", $row)) {
                $create_data["classroom"] = $row["教室名"];
            }
            $search_data = [
                "date" => date("Y-m-d", $date),
                "time" => $row["時限"],
                "target_year" => $row["開講年次"],
                "type_id" => $notify_type->id,
                "lecture_id" => $lecture->id,
            ];
            $notify = LectureNotify::firstOrCreate($search_data, $create_data);
            $notify->histories()->attach($history);
            if ($notify->wasRecentlyCreated) {
                $new_notify->append($notify);
            }
        }
        return $new_notify;
    }
}
