<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use InvalidArgumentException;

class Sendmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sendmail {--from-address=} {--from-name=} {--to=*} {--cc=*} {--bcc=*} {--subject=""} {--mailer=} {body}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            $mailer = Mail::mailer($this->option("mailer"));
        } catch (InvalidArgumentException) {
            return Command::FAILURE;
        }

        $mailer->raw($this->argument("body"), function ($message) {
            $message->to($this->option("to"));
            $message->cc($this->option("cc"));
            $message->bcc($this->option("bcc"));
            if ($this->option("from-address") && $this->option("from-name")) {
                $message->from($this->option("from-address"), $this->option("from-name"));
            }
            $message->subject($this->option("subject"));
        });
        return Command::SUCCESS;
    }
}
