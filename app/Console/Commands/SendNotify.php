<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use InvalidArgumentException;
use Symfony\Component\Mailer\Exception\TransportException;

class SendNotify extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sendnotify {--send} {--from-address=} {--from-name=} {--to-year} {--all} {--subject=""} {--mailer=} {body}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            $mailer = Mail::mailer($this->option("mailer"));
        } catch (InvalidArgumentException) {
            return Command::FAILURE;
        }
        try {
            $mailer->raw($this->argument("body"), function ($message) {
                $message->to([]);
                $emails = [];
                if ($this->option("to-year")) {
                    $emails = User::withWhereHas("lectures", function ($q) {
                        $q->where( "year", 2022);
                    })->get("email")->toArray();
                } elseif ($this->option("all")) {
                    $emails = User::get("email")->toArray();
                    print_r($emails);
                }

                if ($this->option("send")) {
                    $message->bcc($emails);
                }

                if ($this->option("from-address") && $this->option("from-name")) {
                    $message->from($this->option("from-address"), $this->option("from-name"));
                }
                $message->subject($this->option("subject"));
            });
        } catch (TransportException) {
        }


        return Command::SUCCESS;
    }
}
