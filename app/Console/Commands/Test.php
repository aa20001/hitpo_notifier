<?php

namespace App\Console\Commands;

use App\Models\Lecture;
use App\Models\User;
use Illuminate\Console\Command;

class Test extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
//        $lectures = Lecture::with("users")
//            ->where("year", intval(date('Y')) - (intval(date("m"))<=3))
//            ->withExists("users")->select("id")
//        ;
        $users = User::withWhereHas("lectures", function ($q) {
            $q->where( "year", 2022);
        });
        print_r($users->toSql());
        print_r($users->select("name")->get());
        return Command::SUCCESS;
    }
}
