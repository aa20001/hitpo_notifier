<?php

use App\Http\Controllers\Admin\CourseController;
use App\Http\Controllers\CSLectureCacheController;
use App\Http\Controllers\CSLectureHistoryController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\OAuth\CallbackFromProviderController;
use App\Http\Controllers\OAuth\LogoutController;
use App\Http\Controllers\OAuth\RedirectToProviderController;
use App\Http\Controllers\SubscribeController;
use App\Http\Controllers\UnSubscribeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect("home");
});

Route::get('/home', HomeController::class) -> name("home");
Route::get('/CSLecture', CSLectureCacheController::class) -> name("CSLecture");
Route::get('/CSLectureHistory', CSLectureHistoryController::class) -> name("CSLectureHistory");

Route::get('/oauth/login', RedirectToProviderController::class)->name('oauth.login');
Route::get('/oauth/callback', CallbackFromProviderController::class)->name('oauth.callback');
Route::get('/oauth/logout', LogoutController::class)->name('oauth.logout');


Route::middleware(['auth'])->group(function () {
    // ここに書かれたルートに未認証でアクセスすると、ログイン・登録後にちゃんと戻ってくれる
    Route::get("/subscribe", SubscribeController::class) -> name("subscribe");
    Route::get("/unsubscribe", UnSubscribeController::class) -> name("unsubscribe");
    Route::get('/admin/courses', CourseController::class);
});



