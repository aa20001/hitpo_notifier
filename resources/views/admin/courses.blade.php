<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>HITPO Notifier</title>

    <!-- Fonts -->

    <!-- Styles -->
    <link rel="stylesheet" href="/bulma/css/bulma.css">

</head>
<body class="dark" style="width: 100vw; height: 100vh">
<nav class="navbar" role="navigation" aria-label="main navigation">
    <a class="navbar-item" href="{{route("home")}}">HITPO Notifier</a>
    <div class="navbar-end">
        <div class="navbar-item">
            @auth
                <a class="button is-danger" href="{{route("oauth.logout")}}">Logout</a>
            @else
                <a class="button is-primary" href="{{route("oauth.login")}}">Login</a>
            @endauth
        </div>
    </div>
</nav>
<section style="height: 100%">
    <div class="container" style="height: 100%">
        @foreach($courses as $course)
            <p>{{$course->name}}, {{$course->year}}</p>
        @endforeach
    </div>
</section>
</body>
</html>
