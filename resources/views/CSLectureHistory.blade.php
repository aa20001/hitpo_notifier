@php use App\Models\LectureNotifyType; @endphp
@php use App\Models\LectureNotify; @endphp
    <!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>HITPO Notifier</title>

    <!-- Fonts -->

    <!-- Styles -->
    <link rel="stylesheet" href="{{env("APP_URL")}}/bulma/css/bulma.css">

</head>
<body style="width: 100vw; height: 100vh">
@include("common.navbar")
<section style="height: 100%">
    <div class="container" style="height: 100%">
        @auth
            <form action="{{route("CSLectureHistory")}}" method="get">
                <div class="field control is-horizontal is-expanded">
                    <div class="field-body">
                        <div class="field-label is-normal"><label class="label">表示種別: </label></div>
                        <div class="field">
                            <div class="select">
                                <select name="type" class="is-expanded">
                                    @foreach(LectureNotifyType::all() as $_type)
                                        @if($type && $_type->id == $type->id)
                                            <option value="{{$_type->id}}" selected>{{$_type->name}}</option>
                                        @else
                                            <option value="{{$_type->id}}">{{$_type->name}}</option>
                                        @endif

                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="field-label is-normal"><label class="label">表示件数: </label></div>
                        <div class="field">
                            <div class="select">
                                <select name="nrows" class="is-expanded">
                                    @foreach($result_lengths as $length)
                                        @if($n_rows == $length)
                                            <option selected value="{{$length}}">{{$length}}</option>
                                        @else
                                            <option value="{{$length}}">{{$length}}</option>
                                        @endif

                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <button class="button" type="submit">実行</button>
                    </div>
                </div>
            </form>
            @if($notifies)
                @php $classroom_is_notnull = LectureNotify::where(["type_id"=>$type->id])->whereNotNull("classroom")->count(); @endphp
                <table class="table" style="width: 100%">
                    <thead>
                    <tr>
                        <th>id</th>
                        <th>学部</th>
                        <th>学科</th>
                        <th>開講年次</th>
                        <th>日付</th>
                        <th>時限</th>
                        <th>授業名</th>
                        @if($classroom_is_notnull)
                            <th>教室名</th>
                        @endif
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($notifies->get() as $notify)
                        <tr>
                            <th>{{$notify->id}}</th>
                            <td>{{$notify->undergraduate}}</td>
                            <td>{{$notify->department}}</td>
                            <td>{{$notify->target_year}}</td>
                            <td>{{$notify->date}}</td>
                            <td>{{$notify->time}}</td>
                            <td>{{$notify->lecture_name}}</td>
                            @if($classroom_is_notnull)
                                <td>{{$notify->classroom}}</td>
                            @endif
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else
                実行をクリックして検索を実行
            @endif


        @else
            <div class="is-size-1 has-text-centered has-text-danger">Login Required</div>
        @endauth
    </div>
</section>
</body>
</html>
