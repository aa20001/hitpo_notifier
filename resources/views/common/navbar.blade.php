
<script>
    document.addEventListener('DOMContentLoaded', () => {
        // Functions to open and close a modal
        function openModal($el) {
            $el.classList.add('is-active');
        }

        function closeModal($el) {
            $el.classList.remove('is-active');
        }

        function closeAllModals() {
            (document.querySelectorAll('.modal') || []).forEach(($modal) => {
                closeModal($modal);
            });
        }

        // Add a click event on buttons to open a specific modal
        (document.querySelectorAll('.js-modal-trigger') || []).forEach(($trigger) => {
            const modal = $trigger.dataset.target;
            const $target = document.getElementById(modal);

            $trigger.addEventListener('click', () => {
                openModal($target);
            });
        });

        // Add a click event on various child elements to close the parent modal
        (document.querySelectorAll('.modal-background, .modal-close, .modal-card-head .delete, .modal-card-foot .button') || []).forEach(($close) => {
            const $target = $close.closest('.modal');

            $close.addEventListener('click', () => {
                closeModal($target);
            });
        });

        // Add a keyboard event to close all modals
        document.addEventListener('keydown', (event) => {
            const e = event || window.event;

            if (e.keyCode === 27) { // Escape key
                closeAllModals();
            }
        });
    });
</script>
<nav class="navbar" role="navigation" aria-label="main navigation">
    <a class="navbar-item" href="{{route("home")}}">HITPO Notifier</a>
    <a class="navbar-item" href="{{route("CSLecture")}}">休講・補講情報(キャッシュ)</a>
    <a class="navbar-item" href="{{route("CSLectureHistory")}}">休講・補講情報(履歴)</a>
    <div class="navbar-end">
{{--        <a class="navbar-item js-modal-trigger" data-target="usage-modal">?</a>--}}
        <div class="navbar-item">
            @auth
                <a class="button is-danger" href="{{route("oauth.logout")}}">Logout</a>
            @else
                <a class="button is-primary" href="{{route("oauth.login")}}">Login</a>
            @endauth
        </div>
    </div>
</nav>
