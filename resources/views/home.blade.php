<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>HITPO Notifier</title>

    <!-- Fonts -->

    <!-- Styles -->
    <link rel="stylesheet" href="{{env("APP_URL")}}/bulma/css/bulma.css">

</head>
<body style="width: 100vw; height: 100vh">
@include("common.navbar")
<section style="height: 100%">
    <div class="container" style="height: 100%">
        <div id="usage-modal" class="modal">
            <div class="modal-background"></div>

            <div class="modal-content">
                <div class="box">
                    通知の設定方法
                    <ol>
                        <li>入力フォームに授業コードを入力する。<br>
                            授業コードは<a href="https://gitlab.com/aa20001/hitpo-scripts/-/raw/main/copyLectureIds.user.js">ここ</a>のツールを使うと入手できます。<br>
                            ツールを使用するためにはユーザースクリプト用のプラグインが必要です。<br>
                            それ以外の方法ではMy時間割から登録したい授業を開いた時のURLにlecture_id=授業コードの形で含まれます。<br>
                            登録は今年度に対してしか行えません。
                        </li>
                        <li>Subscribeボタンを押すと登録されます。<br>
                        変更通知は学校のメールアドレスcc.it-hiroshima.ac.jpに送信されます。</li>
                    </ol><br>
                    通知の解除方法
                    <ol>
                        <li>このページで解除したい授業を探す</li>
                        <li>UnSubscribeボタンをクリックする。</li>
                    </ol>
                    <!-- Your content -->
                </div>
            </div>

            <button class="modal-close is-large" aria-label="close"></button>
        </div>
        @auth
            <form action="{{route("subscribe")}}" method="get">
                <div class="field is-horizontal">
                    <div class="field-label is-normal"><label class="label">授業コード登録:</label></div>
                    <div class="field-body">
                        <div class="field is-horizontal">
                            <input class="input" type="text" name="code" placeholder="ABISPXXXXX[,ABISPXXXXX...]"/>
                            <button class="button is-primary" type="submit">Subscribe</button>
                        </div>
                    </div>
                </div>

            </form>
            <table class="table" style="width: 100%">
                <thead>
                <tr>
                    <th>授業コード</th>
                    <th>開講年度</th>
                    <th>講義名</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody>
                @foreach($lectures as $lecture)
                    <tr>
                        <td>{{$lecture->code}}</td>
                        <td>{{$lecture->year}}</td>
                        <td>{{$lecture->name}}</td>
                        <td><a class="button is-danger" href="{{route("unsubscribe", ["code" =>$lecture->code])}}">UnSubscribe</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

        @else
            <div class="is-size-1 has-text-centered has-text-danger">Login Required</div>
        @endauth
    </div>
</section>
</body>
</html>
