@php use App\Models\LectureNotifyType; @endphp
@php use App\Models\LectureNotify; @endphp
    <!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>HITPO Notifier</title>

    <!-- Fonts -->

    <!-- Styles -->
    <link rel="stylesheet" href="{{env("APP_URL")}}/bulma/css/bulma.css">

</head>
<body style="width: 100vw; height: 100vh">
@include("common.navbar")
<section style="height: 100%">
    <div class="container" style="height: 100%">
        @auth
            @if($history)
                @php
                    $scrap_time = new DateTime($history->datetime);
                    $scrap_time->setTimeZone(new DateTimeZone('Asia/Tokyo'));
                @endphp
                Scraped at {{$scrap_time->format('Y-m-d H:i:s')}}
                @foreach(LectureNotifyType::groupBy("id")->get() as $lnt)
                    <h1>{{$lnt->name}}</h1>
                    @php
                        $notifies = LectureNotify::where(["type_id"=>$lnt->id])
                    @endphp
                    <table class="table" style="width: 100%">
                        <thead>
                        <tr>
                            <th>学部</th>
                            <th>学科</th>
                            <th>開講年次</th>
                            <th>日付</th>
                            <th>時限</th>
                            <th>授業名</th>
                            @if($notifies->whereNotNull("classroom")->count())
                                <th>教室名</th>
                            @endif
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($history->notifies as $notify)
                            @if($notify->type_id == $lnt->id)
                                <tr>
                                    <td>{{$notify->undergraduate}}</td>
                                    <td>{{$notify->department}}</td>
                                    <td>{{$notify->target_year}}</td>
                                    <td>{{$notify->date}}</td>
                                    <td>{{$notify->time}}</td>
                                    <td>{{$notify->lecture_name}}</td>
                                    @if($notifies->whereNotNull("classroom")->count())
                                        <td>{{$notify->classroom}}</td>
                                    @endif
                                </tr>
                            @endif
                        @endforeach
                        </tbody>
                    </table>
                @endforeach
            @else
                NoCached
            @endif

        @else
            <div class="is-size-1 has-text-centered has-text-danger">Login Required</div>
        @endauth
    </div>
</section>
</body>
</html>
